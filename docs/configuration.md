# Configuration

BlueLabs ID can be configured through Elixir config. This is the default config:

```elixir
config :blue_labs,
  time_resolution: :microsecond,
  time_bits: 52,
  alphabet: '0123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz',
  start_time: 1_578_044_200_335_382,
  telemetry: false
```

_Important note:_ the configuration is read at compile time and any changes during the runtime
will not be reflected (if you are curious how to change config during runtime see
[Releases](https://hexdocs.pm/mix/Mix.Tasks.Release.html)). The reason is that any change to the
config will lead to creating new series of IDs which won't be compatible with the old ones. So if
you really wanna change config stop the running application, adapt your code to the new IDs,
change the config and start you application again.

## Time resolution

`:time_resolution` - `:second` or `:millisecond` or `:microsecond` (default) or `:nanosecond`

This config configures the resolution of the "time" portion of the ID. Changing this setting has
two effects on the generated IDs:

 1. The IDs generated within this time period are not guaranteed to be sorted between each other
 2. Together with "Time bits" config it dictates how long will this series of IDs will be
    sortable.

IDs generated with different time_resolution will not correctly sort between each other so it is
not advised to change this setting in running application unless you 100 % know what you are
doing.

## Time bits

`:time_bits` - Integer, greater than 0 and equal or lower than 64

This config configures the "time" proportion of the ID (if you wanna know more about the algorithm
generating the ID and the "time" proportion is read [Algorithm](algorithm.html)). This is pretty
important setting since it directly sets how long the "sorting" feature of the IDs will work. Here
are some examples:

"Time resolution" = `:nanosecond`
    52 = 2 ^ 52 nanoseconds ~= 52 days
    58 = 2 ^ 58 nanoseconds ~= 9.1 years
    62 = 2 ^ 62 nanoseconds ~= 146 years

"Time resolution" = `:microsecond`
    44 = 2 ^ 44 microseconds ~= 203 days
    48 = 2 ^ 48 microseconds ~= 8.9 years
    52 = 2 ^ 52 microseconds ~= 142 years (default)
    58 = 2 ^ 58 microseconds ~= 2283 years

"Time resolution" = `:milisecond`
    32 = 2 ^ 32 miliseconds ~= 50 days
    36 = 2 ^ 36 miliseconds ~= 2.2 years
    40 = 2 ^ 40 miliseconds ~= 34.8 years
    44 = 2 ^ 44 miliseconds ~= 557 years
    48 = 2 ^ 48 miliseconds ~= 8919 years

"Time resolution" = `:second`
    20 = 2 ^ 20 seconds ~= 12 days
    24 = 2 ^ 24 seconds ~= 194 days
    28 = 2 ^ 28 seconds ~= 8.5 years
    32 = 2 ^ 32 seconds ~= 136 years
    36 = 2 ^ 36 seconds ~= 2178 years

So why not to set it to 64 and forget about it? The answer is collisions. If you set `:time_bits`
to 64 you will make whole ID basically just time - this will allow you to run it for cca 584 554
years (in `:microsecond` resolution) but also create a lot of collisions beacuse every ID
generated in the same microsecond will be exactly the same.

So how likely I get collision with different settings? Good question! For example when using
default setting `52` the last 12 bits are filled with random bits - which is 2 ^ 12 = 4096
different options. This means 1 in 4096 chance (~= 0.024 %) to have collision between two IDs
generated in the same microsecond. But this chances raises quickly - if you generate 76 IDs in
the same microsecond the chance that there will be collision between any of that two IDs is cca
50.3 % (if you wanna know how I get this number see
[Birthday Problem](https://en.wikipedia.org/wiki/Birthday_problem)).

So what setting is best for you? If your usecase requires the sorting mechanism to work for
a really long time and you don't expect to generate a lot of IDs in a lot of concurrent
processes you can set your time bits pretty high (but do you really need more than 150 years of
consistency?). If you on the other hand need to generate a lot of IDs in quick succession and
in lot of concurrent processes, than consider lowering the time bits to 48 or even lower. If you
need to generate lot of IDs and you also require it to run for a long time but you don't need
strong consistency when sorting you can set `:time_resolution` to `:second` and `:time_bits` to 32
that way you will get probability of collision bigger than 50 % once you generate more than 77164
IDs in one second.

And finally if you require everything you should probalby use something else which uses more than
64 bits of data ([UUID](https://en.wikipedia.org/wiki/Universally_unique_identifier) seems like
great option) but remember that you will loose the "user friendly" feature because those IDs are
quite long (to mitigate the collision problem).

IDs generated with different time_bits will not correctly sort between each other so it is not
advised to change this setting in running application unless you 100 % know what you are doing.

## Start time

`:start_time` - Integer, Unix time in microseconds

Another really important config. This value sets the start of the counter for your time period
set by `:time_bits` option. You should never change this value once you set it for the first
time. If you change this value all IDs generated with previous value will not correctly sort
with newly generated IDs.

To find correct value you can use included mix command:

```bash
mix blue_labs.time
```

## Alphabet

`:alphabet` - Charlist

This charlist is used for ID encoding which means only those characters included here can appear
in the IDs. The order of this characters matters - it should be sorted according to your sorting
mechanism for IDs. This means that if you are sorting based on ASCII table the characters in the
`:alphabet` setting should be in the order as they appear in the ASCII table. By default the
alphabet contains all alphanumeric characters except "O" and "o" to avoid confusion with "0" and
with each other and "l" and "I" to avoid confusion with "1" and with each other.

You can change this setting to achieve this results:
  * different "user friendly" requirements (you want to also remove "0" and "1" to be 100 %
    sure)
  * different sorting mechanism so you need different order of the chars in the alphabet

Keep in mind that this setting is used only when encoding in `:custom_alphabet` format
(default).

If you don't care about the "user friendly" features don't add the whole alphabet but use
`:base64` format because it will be more performant.

IDs generated with different alphabet setting will not correctly sort between each other so
don't change it in already running application.

## Telemetry

`:telemetry` - boolean

This config specifies if the telemetry measurements should be included in compiled code. The
default value is `false`. If you want to use telemetry keep in mind these two things:

  * Adding telemetry roughly doubles the execution time for ID generation. With telemetry it is
    roughly 2 microseconds which is way bellow the acceptance treshold but if you need as much
    throughput as possible you should probably disable it.
  * `:telemetry` is optional dependency so if you are going to enable it add it to your `mix.exs`
    file as dependency

    ```elixir
    [
      {:telemetry, "~> 0.4"},
    ]
    ```

If you want to know more about telemetry read [Telemetry](telemetry.html) page.
