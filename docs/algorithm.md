# Algorithm

The ID itself is strictly defined as binary but because we rarely need the ID in this form the
encoding to different string representations is also part of the specification and library.

## ID generation

Binary ID is 64 bits long and consists of two part the `time_bits` and the `random_bits`. The
proportion between those bits is configurable by the config. By default the proportion is 52:12.

```
|    52 time bits         | 12  |
|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|_|
```

### Time bits

They are the most significant bits in the ID.

You get bits by getting the current Unix time in microseconds (`:os.system_time(:microsecond)`),
then extract `start_time` number from config and then taking desired amount (`time_bits` setting)
of least significant bits from that number.

```elixir
# Get current time in microseconds
iex> :os.system_time(:microsecond)
1578400996045434

# Substract the default start time
iex> 1578400996045434 - 1578044200335382
356795710052

# See all bytes in the number
iex> :binary.encode_unsigned(356795710052)
<<83, 18, 174, 122, 100>>

# Get last 52 bits
iex> <<356795710052::52>>
<<0, 5, 49, 42, 231, 166, 4::size(4)>>
```

### Random bits

They are the least significant bits in the ID.

The bits should be random but don't have to be strong random bits since they are not used in
security scenario so I choosed to use `:rand` library instead of `:crypto` from Erlang. Generate
64 random bits and than get desired amount of bits (`64 - time_bits`) for ID.

In the library the random number between 0 and 18446744073709551615 (max unsigned integer in 64
bits) with `:rand.uniform/1`.

```elixir
# Get max 64-bit unsigned integer
iex> :binary.decode_unsigned(<<255, 255, 255, 255, 255, 255, 255, 255>>)
18446744073709551615
iex> String.to_integer("FFFFFFFFFFFFFFFF", 16)
18446744073709551615

# Get random number
iex> :rand.uniform(18446744073709551615)
703908893840789156

# Get binary representation of the number
iex> :binary.encode_unsigned(703908893840789156)
<<9, 196, 201, 104, 211, 158, 78, 164>>

# Get last 12 bits
iex> <<703908893840789156::12>>
<<234, 4::size(4)>>
```

## ID encoding

The raw binary can be encoded in a lot of different ways. This library provides these options:

### Integer encoding

Representation of the binary as unsigned integer. E.g.:

```
1100 = 12
1010 0011 = 163
1111 1111 = 255
```

Using `:binary.decode_unsigned/1` function from Erlang.

### Hexadecimal encoding

Encode binary to hexadecimal number.

```
1100 = C
1010 0011 = A3
1111 1111 = FF
```

Using `Base.encode16/1` function.

### Base64 encoding

Encode binary with Base64 encoding.

```
1100 = DA
1010 0011 = ow
1111 1111 = /w
```

Using `Base.encode64/1` function.

### URL safe Base64 encoding

Encode binary with Base64 with URL safe characters.

```
1100 = DA
1010 0011 = ow
1111 1111 = _w
```

Using `Base.url_encode64/1` function.

### Custom alphabet encoding



```
1100 = C
1010 0011 = 2p
1111 1111 = 4P
```
