# Telemetry

This library optionaly includes telemetry measurements using
[`:telemetry`](https://github.com/beam-telemetry/telemetry) library. By default it is turned off
but you can turn it on in config (for more info see [Configuration](configuration.html) page).

These telemetry events are emitted:
  * `[:blue_labs, :generate, :start]`
  * `[:blue_labs, :generate, :stop]`
  * `[:blue_labs, :encode, :start]`
  * `[:blue_labs, :encode, :stop]`

## Events

### Start event

This event is emited before the generation of the ID starts. It includes measurement in this form:

```elixir
%{
  time: System.monotonic_time(:nanosecond)
}
```

and in case of `:encode` event metadata with format specification:

```elixir
%{format: :integer | :hex | :base64 | :url | :custom_alphabet}
```

### Stop event

This event is emitted emediately after the ID is generated and encoded. It includes measurement in
this form:

```elixir
%{
  time: System.monotonic_time(:nanosecond),
  duration: System.monotonic_time(:nanosecond)
}
```

and in case of `:encode` event metadata with format specification:

```elixir
%{format: :integer | :hex | :base64 | :url | :custom_alphabet}
```

## Usage

First of all you need to add `:telemetry` app to your dependencies:

```elixir
[
  {:telemetry, "~> 0.4"},
]
```

If you want to listen to the events you have attach handlers to those events. First create
handler:

```elixir
defmodule MyApp.EventHandler do
  require Logger

  def handle([:blue_labs, name, :start], %{time: time}, _metadata, _config) do
    Logger.info("#{Atom.to_string(name)} started at #{time}")
  end

  def handle([:blue_labs, name, :stop], %{time: time, duration: duration}, _metadata, _config) do
    Logger.info("#{Atom.to_string(name)} finished in #{duration} nanoseconds (ended at #{time})")
  end
end
```

and than attach it when your application starts:

```elixir
defmodule MyApp.Application do
  use Application

  alias MyApp.EventHandler

  ...

  def start(_type, _args) do
    children = [
      ...
    ]

    :telemetry.attach_many(
      "blue-labs-handler",
      BlueLabs.telemetry_events(),
      &EventHandler.handle/4,
      []
    )

    opts = [strategy: :one_for_one, name: MyApp.Supervisor]
    Supervisor.start_link(children, opts)
  end

  ...
end
```

This way you will see the events logged in the console.
