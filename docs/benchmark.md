# Benchmark

You can run benchmark as mix task:

```bash
$ mix benchmark
```

and profiling like this:

```bash
$ mix profile
```

## Optimization process

I started optimizations from commit
[0aa840f7](https://gitlab.com/Sgiath/bluelabs-id/tree/0aa840f75b9d8d52314c76e6729d89ab76634f36)

This is initial benchmark:

```
Operating System: Linux
CPU Information: Intel(R) Core(TM) i7-6700K CPU @ 4.00GHz
Number of Available Cores: 8
Available memory: 15.57 GB
Elixir 1.9.4
Erlang 22.2.2

Benchmark suite executing with the following configuration:
warmup: 2 s
time: 5 s
memory time: 0 ns
parallel: 1
inputs: none specified
Estimated total run time: 42 s

Name                      ips        average  deviation         median         99th %
binary              1068.29 K        0.94 μs  ±2849.25%        0.78 μs        1.78 μs
integer             1051.95 K        0.95 μs  ±2784.90%        0.82 μs        1.78 μs
hex                  722.79 K        1.38 μs  ±1428.33%        1.19 μs        2.39 μs
url                  670.75 K        1.49 μs  ±1249.63%        1.31 μs        2.89 μs
base64               665.06 K        1.50 μs  ±1087.93%        1.32 μs        2.84 μs
custom_alphabet      327.84 K        3.05 μs   ±554.57%        2.75 μs        5.87 μs

Comparison:
binary              1068.29 K
integer             1051.95 K - 1.02x slower +0.0145 μs
hex                  722.79 K - 1.48x slower +0.45 μs
url                  670.75 K - 1.59x slower +0.55 μs
base64               665.06 K - 1.61x slower +0.57 μs
custom_alphabet      327.84 K - 3.26x slower +2.11 μs

Extended statistics:

Name                    minimum        maximum    sample size                     mode
binary                  0.69 μs    26452.67 μs         3.50 M                  0.73 μs
integer                 0.70 μs    25741.87 μs         3.47 M                  0.83 μs
hex                     1.07 μs    18492.37 μs         2.59 M                  1.20 μs
url                     1.15 μs    17507.78 μs         2.42 M                  1.32 μs
base64                  1.17 μs    15156.15 μs         2.42 M                  1.33 μs
custom_alphabet            2 μs    10305.29 μs         1.40 M                  2.68 μs
```

The results from the initial implementation shows that custom_alphabet encoding takes much longer
time than build-in options. This is somewhat expected because the implementation needs to be
generic enough to allow for custom alphabet length. But the difference seemed too high. I wanted
to investigate so I run profiling on the `BlueLabs.generate/0` function. Here is the result:

```
#                                               CALLS     % TIME µS/CALL
Total                                             417 100.0   67    0.16
:erlang.unique_integer/0                            1  0.00    0    0.00
:erlang.system_time/0                               1  0.00    0    0.00
:lists.keyfind/3                                    1  0.00    0    0.00
:binary.decode_unsigned/1                           1  0.00    0    0.00
anonymous fn/1 in :rand.mk_alg/1                    1  0.00    0    0.00
anonymous fn/2 in :rand.mk_alg/1                    1  0.00    0    0.00
anonymous fn/1 in :rand.mk_alg/1                    1  0.00    0    0.00
:rand.exsss_next/1                                  1  0.00    0    0.00
:rand.seed_get/0                                    1  0.00    0    0.00
:rand.seed_put/1                                    2  0.00    0    0.00
:rand.uniform/1                                     1  0.00    0    0.00
:rand.seed_s/2                                      1  0.00    0    0.00
:rand.seed_s/1                                      1  0.00    0    0.00
:rand.seed/1                                        1  0.00    0    0.00
Enum.at/2                                           9  0.00    0    0.00
anonymous fn/0 in :elixir_compiler_1.__FILE__/1     1  0.00    0    0.00
BlueLabs.generate/1                                 1  0.00    0    0.00
BlueLabs.generate/0                                 1  0.00    0    0.00
List.to_string/1                                    1  0.00    0    0.00
:unicode.characters_to_binary/1                     1  0.00    0    0.00
BlueLabs.Encoder.encode_integer/1                   1  0.00    0    0.00
BlueLabs.Encoder.encode_alphabet_rec/1              1  0.00    0    0.00
BlueLabs.Encoder.encode_alphabet/1                  1  0.00    0    0.00
BlueLabs.Encoder.encode/2                           1  0.00    0    0.00
BlueLabs.Generator.generate/0                       1  0.00    0    0.00
:maps.get/3                                         1  0.00    0    0.00
Keyword.get/3                                       1  0.00    0    0.00
:erlang.phash2/1                                    1  1.49    1    1.00
:erlang.put/2                                       2  1.49    1    0.50
:rand.seed58/1                                      3  1.49    1    0.33
:rand.exsss_uniform/2                               1  1.49    1    1.00
:rand.mk_alg/1                                      1  1.49    1    1.00
:rand.uniform_s/2                                   1  1.49    1    1.00
:rand.uniform_range/7                               2  1.49    1    0.50
:erlang.apply/2                                     1  1.49    1    1.00
Enum.slice_any/3                                    9  1.49    1    0.11
Enum.at/3                                           9  1.49    1    0.11
BlueLabs.Encoder.encode_char/1                      9  1.49    1    0.11
BlueLabs.Encoder.div_rem/1                          9  1.49    1    0.11
:os.system_time/1                                   1  2.99    2    2.00
:rand.exsss_seed/1                                  1  2.99    2    2.00
:rand.uniform_range/4                               1  4.48    3    3.00
BlueLabs.Encoder.encode_alphabet_rec/2             10  4.48    3    0.30
:rand.splitmix64_next/1                             3  5.97    4    1.33
:unicode.characters_to_binary/2                     1 11.94    8    8.00
Enumerable.List.slice/3                           316 49.25   33    0.10

Profile done over 46 matching functions
```

Which shows that almost 50 % of the execution time is spend in `List.slice/3` function which is
called 316 times. After more investigation why am I using this function I figured out that it is
part of the `Enum.at/3` function which I am using to get the appropriate character in alphabet.
This must be because of Elixir's linked lists so I changed the `List` to the Erlang's `:array`
which has constant access time (commit
[e0505442](https://gitlab.com/Sgiath/bluelabs-id/tree/e0505442fbac67f13a900aff691812debee34a95))
and here are the results:

```
Operating System: Linux
CPU Information: Intel(R) Core(TM) i7-6700K CPU @ 4.00GHz
Number of Available Cores: 8
Available memory: 15.57 GB
Elixir 1.9.4
Erlang 22.2.2

Benchmark suite executing with the following configuration:
warmup: 2 s
time: 5 s
memory time: 0 ns
parallel: 1
inputs: none specified
Estimated total run time: 42 s

Name                      ips        average  deviation         median         99th %
binary              1162.21 K        0.86 μs  ±1713.97%        0.74 μs        1.41 μs
integer             1124.59 K        0.89 μs  ±1607.60%        0.82 μs        1.35 μs
hex                  783.29 K        1.28 μs  ±1212.59%        1.12 μs        2.27 μs
url                  738.58 K        1.35 μs   ±993.18%        1.21 μs        2.27 μs
base64               725.23 K        1.38 μs  ±1009.69%        1.22 μs        2.42 μs
custom_alphabet      570.70 K        1.75 μs   ±964.47%        1.56 μs        2.76 μs

Comparison:
binary              1162.21 K
integer             1124.59 K - 1.03x slower +0.0288 μs
hex                  783.29 K - 1.48x slower +0.42 μs
url                  738.58 K - 1.57x slower +0.49 μs
base64               725.23 K - 1.60x slower +0.52 μs
custom_alphabet      570.70 K - 2.04x slower +0.89 μs

Extended statistics:

Name                    minimum        maximum    sample size                     mode
binary                  0.68 μs    14468.58 μs         3.77 M                  0.72 μs
integer                 0.69 μs    14681.47 μs         3.68 M                  0.83 μs
hex                     1.02 μs    12914.75 μs         2.80 M                  1.11 μs
url                     1.12 μs    11038.58 μs         2.66 M                  1.20 μs
base64                  1.12 μs    12204.32 μs         2.62 M                  1.21 μs
custom_alphabet         1.49 μs    10737.32 μs         2.25 M                  1.55 μs
```

This looks much better! But I wanted to be sure so here is the profiling again:

```
#                                               CALLS     % TIME µS/CALL
Total                                              81 100.0   33    0.41
:erlang.unique_integer/0                            1  0.00    0    0.00
:erlang.system_time/0                               1  0.00    0    0.00
:binary.decode_unsigned/1                           1  0.00    0    0.00
anonymous fn/1 in :rand.mk_alg/1                    1  0.00    0    0.00
anonymous fn/2 in :rand.mk_alg/1                    1  0.00    0    0.00
anonymous fn/1 in :rand.mk_alg/1                    1  0.00    0    0.00
:rand.seed58/1                                      3  0.00    0    0.00
:rand.exsss_next/1                                  1  0.00    0    0.00
:rand.seed_get/0                                    1  0.00    0    0.00
:rand.seed_put/1                                    2  0.00    0    0.00
:rand.uniform/1                                     1  0.00    0    0.00
:rand.seed_s/1                                      1  0.00    0    0.00
:rand.seed/1                                        1  0.00    0    0.00
anonymous fn/0 in :elixir_compiler_1.__FILE__/1     1  0.00    0    0.00
BlueLabs.generate/1                                 1  0.00    0    0.00
BlueLabs.generate/0                                 1  0.00    0    0.00
List.to_string/1                                    1  0.00    0    0.00
:unicode.characters_to_binary/1                     1  0.00    0    0.00
BlueLabs.Encoder.encode_alphabet_rec/1              1  0.00    0    0.00
BlueLabs.Encoder.encode/2                           1  0.00    0    0.00
BlueLabs.Generator.generate/0                       1  0.00    0    0.00
:maps.get/3                                         1  0.00    0    0.00
Keyword.get/3                                       1  0.00    0    0.00
:erlang.phash2/1                                    1  3.03    1    1.00
:erlang.put/2                                       2  3.03    1    0.50
:lists.keyfind/3                                    1  3.03    1    1.00
:rand.exsss_uniform/2                               1  3.03    1    1.00
:rand.mk_alg/1                                      1  3.03    1    1.00
:rand.uniform_s/2                                   1  3.03    1    1.00
:rand.seed_s/2                                      1  3.03    1    1.00
:rand.uniform_range/7                               2  3.03    1    0.50
:erlang.apply/2                                     1  3.03    1    1.00
:array.get/2                                        9  3.03    1    0.11
:os.system_time/1                                   1  6.06    2    2.00
:rand.uniform_range/4                               1  6.06    2    2.00
:array.get_1/3                                     18  6.06    2    0.11
BlueLabs.Encoder.encode_alphabet_rec/2             10  6.06    2    0.20
:rand.exsss_seed/1                                  1  9.09    3    3.00
:rand.splitmix64_next/1                             3 12.12    4    1.33
:unicode.characters_to_binary/2                     1 24.24    8    8.00
```

This looks better but `:unicode.characters_to_binary/2` is called only once but almost one quarter
of the time is spend in that function. This is weird, especially since I don't know why I would
need to call such a function. I did a bit of investigation again and I found the suspect -
`List.to_string/1`. And sure enough passing a list of single characters in recursive function just
to join them at the end seems unnecesary. So I changed it work with binary all the time (commit
[66d072bf](https://gitlab.com/Sgiath/bluelabs-id/tree/66d072bf83d4328407daa45984920336bd59b4e0)).
Here is benchmark and profiling output:

```
Operating System: Linux
CPU Information: Intel(R) Core(TM) i7-6700K CPU @ 4.00GHz
Number of Available Cores: 8
Available memory: 15.57 GB
Elixir 1.9.4
Erlang 22.2.2

Benchmark suite executing with the following configuration:
warmup: 2 s
time: 5 s
memory time: 0 ns
parallel: 1
inputs: none specified
Estimated total run time: 42 s

Name                      ips        average  deviation         median         99th %
binary              1119.09 K        0.89 μs  ±1688.17%        0.83 μs        1.35 μs
integer             1109.59 K        0.90 μs  ±1487.52%        0.83 μs        1.41 μs
hex                  758.47 K        1.32 μs  ±1105.20%        1.15 μs        2.39 μs
base64               706.24 K        1.42 μs   ±790.00%        1.27 μs        2.51 μs
url                  696.27 K        1.44 μs   ±952.04%        1.28 μs        2.55 μs
custom_alphabet      559.02 K        1.79 μs   ±469.15%        1.69 μs        3.10 μs

Comparison:
binary              1119.09 K
integer             1109.59 K - 1.01x slower +0.00765 μs
hex                  758.47 K - 1.48x slower +0.42 μs
base64               706.24 K - 1.58x slower +0.52 μs
url                  696.27 K - 1.61x slower +0.54 μs
custom_alphabet      559.02 K - 2.00x slower +0.90 μs

Extended statistics:

Name                    minimum        maximum    sample size                     mode
binary                  0.69 μs    14625.47 μs         3.68 M                  0.83 μs
integer                 0.70 μs    12880.88 μs         3.65 M                  0.83 μs
hex                     1.06 μs    10795.12 μs         2.74 M                  1.14 μs
base64                  1.16 μs     8427.91 μs         2.57 M                  1.26 μs
url                     1.17 μs     9421.06 μs         2.56 M                  1.27 μs
custom_alphabet         1.60 μs     7048.84 μs         2.18 M                  1.68 μs
```

```
#                                               CALLS     % TIME µS/CALL
Total                                              78 100.0   34    0.44
:erlang.unique_integer/0                            1  0.00    0    0.00
:erlang.system_time/0                               1  0.00    0    0.00
:lists.keyfind/3                                    1  0.00    0    0.00
:binary.decode_unsigned/1                           1  0.00    0    0.00
anonymous fn/1 in :rand.mk_alg/1                    1  0.00    0    0.00
anonymous fn/2 in :rand.mk_alg/1                    1  0.00    0    0.00
anonymous fn/1 in :rand.mk_alg/1                    1  0.00    0    0.00
:rand.exsss_next/1                                  1  0.00    0    0.00
:rand.seed_get/0                                    1  0.00    0    0.00
:rand.seed_put/1                                    2  0.00    0    0.00
:rand.uniform/1                                     1  0.00    0    0.00
:rand.seed_s/2                                      1  0.00    0    0.00
:rand.seed_s/1                                      1  0.00    0    0.00
:rand.seed/1                                        1  0.00    0    0.00
anonymous fn/0 in :elixir_compiler_1.__FILE__/1     1  0.00    0    0.00
BlueLabs.generate/1                                 1  0.00    0    0.00
BlueLabs.generate/0                                 1  0.00    0    0.00
BlueLabs.Encoder.encode_alphabet_rec/1              1  0.00    0    0.00
BlueLabs.Encoder.encode/2                           1  0.00    0    0.00
BlueLabs.Generator.generate/0                       1  0.00    0    0.00
:maps.get/3                                         1  0.00    0    0.00
Keyword.get/3                                       1  0.00    0    0.00
:erlang.phash2/1                                    1  2.94    1    1.00
:erlang.put/2                                       2  2.94    1    0.50
:rand.seed58/1                                      3  2.94    1    0.33
:rand.exsss_uniform/2                               1  2.94    1    1.00
:rand.mk_alg/1                                      1  2.94    1    1.00
:rand.uniform_s/2                                   1  2.94    1    1.00
:rand.uniform_range/7                               2  2.94    1    0.50
:array.get/2                                        9  2.94    1    0.11
:os.system_time/1                                   1  5.88    2    2.00
:array.get_1/3                                     18  5.88    2    0.11
:rand.uniform_range/4                               1  8.82    3    3.00
BlueLabs.Encoder.encode_alphabet_rec/2             10  8.82    3    0.30
:rand.splitmix64_next/1                             3 11.76    4    1.33
:erlang.apply/2                                     1 14.71    5    5.00
:rand.exsss_seed/1                                  1 20.59    7    7.00
```

The benchmark doesn't seems to change much so I created new bechmark testing just this aspect of
the code and suprisingly the `List` seems to be faster.

```bash
$ mix bench.list_vs_binary
```

So I reverted code back to the List. At the end I added few inline compile directives just for a
better feeling :)
