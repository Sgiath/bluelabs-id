# Installation

The package can be installed as Hex package:

1. Add `:blue_labs` to your list of dependencies in `mix.exs`:

    ```elixir
    def deps do
      [
        {:blue_labs, "~> 0.1.0"},
      ]
    end
    ```

2. Run `mix deps.get` to fetch the package from hex
