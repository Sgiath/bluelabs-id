# Rationale

Since unique values needs to be ensured in distributed environment without process coordination
the ID needs to be based on the current time.

Since unique values needs to be ensured even when running paralel in multiple processes ID needs
to contain also some "process identifier" to distinguise two IDs generated at the same time by two
different processes.

64-bits are pretty low so there will always be some tradeoff. Specifically between how long do you
expect to generate IDs while the sort mechanism will still work and how many concurrent processes
you want to run (ratio between time bits and random bits).

This is very similar to
[UUIDv1](https://en.wikipedia.org/wiki/Universally_unique_identifier#Version_1_(date-time_and_MAC_address))
since it uses current time expressed in 100 intervals of nanoseconds since midnight 15 October
1582 UTC - 60 bits - and 48-bit MAC address of the "node" where UUID was generated. I had to edit
it because this is too much bits for my task (60 + 48 = 108 bits but I can use just 64) and also
because in Elixir you can run multiple "nodes" in one machine with one MAC address which could
cause collisions if the two ID would be generated at the same time. I choose to do this edits to
the UUIDv1 specs:

 * Current time has precission only to microseconds - this will lower time bits needed for ID
   while still meeting the requirement of "few thousand per second" IDs
 * Time begining is not midnight 15 October 1582 but it is customizable by settings so every
   application can have different begining of time
 * I generate few random bits for every process running ID generator and use them instead of MAC
   address of the underlying system

This forced me to design it using `GenServer`s so every `GenServer` can have random bits generated
and assigned upon start. The ratio between `time_bits` and `random_bits` is configurable and final
ID is than custom encoded with configurable alphabet so the final ID is as short as possible.

This version can be found in commit
[89e8753a](https://gitlab.com/Sgiath/bluelabs-id/tree/89e8753aed53c2f7701aaae6bcf63e6fd37f27c7)

But this implementation has few problems:
 * The need to start `GenServer` before you can generate ID is not great developer experience
 * If you want to start multiple instances and than "just generate ID" without worring about which
   `GenServer` is called requires custom solution to be written
 * In default configuration there is just 12 bits left for the random "process identifier" which
   allows 2^12 = 4096 possible values. This isn't terribly low but on the other hand the
   probability of the same random bits for two processes will be more than 50 % if we start 76 or
   more processes (see [Birthday Problem](https://en.wikipedia.org/wiki/Birthday_problem)). This
   is quite a lot processes but the problem is that once we manage to generate two processes with
   same random bits we will have increased chance of collisions until at least one of the process
   will die. This can be worked around by assigning process IDs ("random bits") manually to the
   `GenServer`s (which this implementation allows) but it won't be developer friendly solution and
   it will create more code and more bugs in the system using this library.
 * If the generation of the ID would take less than one microsecond (not the case on my machine)
   there would be guaranteed collisions in one `GenServer`.

I decided to simplify the library and generate new `random_bits` for every ID. This way I don't
need to use `GenServer` there can be just simple function call and the probability of collisions
should be fairy low.

This version can be found in commit
[824e6661](https://gitlab.com/Sgiath/bluelabs-id/tree/824e66618b6225b6ed8c073a34bc95e14c209667)

After the initial implementation I realized that not every system will probably need such strong
consistency for sorting the IDs (IDs are currently guaranteed to be sorted unless they are
generated in the same microsecond) so I realized that option to lower time resolution in cost of
sorting consistency wouldn't be a bad idea.

So I introduced new config `:time_resolution`.

To learn how final algorithm works read next page [Algorithm](algorithm.html).
