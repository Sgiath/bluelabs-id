defmodule BlueLabs.MixProject do
  use Mix.Project

  def project do
    [
      # App settings
      app: :blue_labs,
      version: "0.1.0",
      description: description(),
      package: package(),

      # Elixir settings
      elixir: "~> 1.10",
      compilers: [:rustler] ++ Mix.compilers(),
      start_permanent: Mix.env() == :prod,
      consolidate_protocols: Mix.env() != :test,
      deps: deps(),
      aliases: aliases(),

      # Docs
      name: "BlueLabs ID",
      source_url: "https://gitlab.com/Sgiath/bluelabs-id",
      homepage_url: "https://sgiath.dev/projects/blue-labs-id",
      docs: docs(),

      # Tasks and aliases
      aliases: aliases(),
      preferred_cli_env: [
        benchmark: :bench,
        "bench.list_access_time": :bench,
        "bench.list_vs_binary": :bench,
        docs: :docs
      ],

      # Dialyzer
      dialyzer: [
        plt_add_apps: [:mix],
        plt_file: {:no_warn, "priv/plts/dialyzer.plt"}
      ],

      # Rustler
      rustler_crates: [blue_labs: []]
    ]
  end

  def application do
    []
  end

  defp deps do
    [
      # Rust NIFs integration
      {:rustler, "~> 0.21"},

      # Telemetry
      {:telemetry, "~> 0.4", optional: true},

      # Benchmark
      {:benchee, "~> 1.0", only: :bench},

      # Dev libraries
      {:credo, "~> 1.4", only: [:dev, :test], runtime: false},
      {:dialyxir, "~> 1.0", only: :dev, runtime: false},

      # Documentation
      {:ex_doc, "~> 0.22", only: :docs, runtime: false}
    ]
  end

  defp aliases do
    [
      benchmark: "run bench/formats.exs",
      "bench.list_vs_binary": "run bench/list_vs_binary.exs",
      "bench.list_access_time": "run bench/list_access_time.exs",
      profile: "profile.eprof -e 'BlueLabs.generate()'"
    ]
  end

  defp description do
    """
    Unique, user-friendly, sortable IDs.

    BlueLabs interview project.
    """
  end

  defp package do
    [
      name: "blue_labs",
      licenses: ["MIT"],
      links: %{
        "GitLab" => "https://gitlab.com/Sgiath/bluelabs-id",
        "GitHub (mirror)" => "https://github.com/Sgiath/BlueLabs-ID",
        "Homepage" => "https://sgiath.dev/projects/blue-labs-id",
        "Docs" => "https://hexdocs.pm/blue_labs"
      }
    ]
  end

  defp docs do
    [
      main: "BlueLabs",
      authors: [
        "Sgiath <FilipVavera@sgiath.dev>"
      ],
      api_reference: false,
      extra_section: "Docs",
      extras: [
        "docs/installation.md",
        "docs/configuration.md",
        "docs/telemetry.md",
        "docs/benchmark.md",
        "docs/overview.md",
        "docs/rationale.md",
        "docs/algorithm.md"
      ],
      groups_for_extras: [
        Practical: [
          "docs/installation.md",
          "docs/configuration.md",
          "docs/telemetry.md",
          "docs/benchmark.md"
        ],
        Theoretical: [
          "docs/overview.md",
          "docs/rationale.md",
          "docs/algorithm.md"
        ]
      ],
      groups_for_modules: [
        "Public API": [BlueLabs],
        "Experimental API": [BlueLabs.Native],
        Internal: [BlueLabs.Generator, BlueLabs.Encoder, BlueLabs.Telemetry]
      ]
    ]
  end
end
