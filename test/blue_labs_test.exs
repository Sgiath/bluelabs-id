defmodule BlueLabsTest do
  use ExUnit.Case

  doctest BlueLabs.Encoder

  test "not same" do
    id1 = BlueLabs.generate!()
    id2 = BlueLabs.generate!()

    refute id1 == id2
  end

  test "sorted" do
    ids = Enum.map(0..2_000, fn _ -> BlueLabs.generate!() end)

    assert ids == Enum.sort(ids)
  end
end
