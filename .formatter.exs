[
  inputs: ["{mix,.formatter}.exs", "{config,lib,test}/**/*.{ex,exs}"],
  rename_deprecated_at: "1.10.0",
  force_do_end_blocks: false
]
