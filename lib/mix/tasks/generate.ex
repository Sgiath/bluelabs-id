defmodule Mix.Tasks.BlueLabs.Generate do
  @moduledoc ~S"""
  This task is intended for easy to use manual ID generation.

  Can be run without parameters to generate one ID like this:

      $ mix blue_labs.generate
      9hQrn3m9X

  Or with one parameter - number indicating how many IDs should be generated:

      $ mix blue_labs.generate 5
      9hWTiBctN
      9hWTik4MB
      9hWTikEBL
      9hWTikJSX
      9hWTikN4m
      9hWTikTWJ

  Or you can specify the output format of the ID with --format flag:

      $ mix blue_labs.generate --format hex
      $ mix blue_labs.generate --format base64 20

  Available formats are all formats except "binary" since there is not a good way how to output
  it to the console.

  You can write the IDs into file like this:

      $ mix blue_labs.generate 2000 > ids.txt

  """
  @moduledoc authors: ["Sgiath <FilipVavera@sgiath.dev>"]

  use Mix.Task

  @allowed_formats ["integer", "hex", "base64", "url", "custom_alphabet"]

  @shortdoc "Generate one or multiple IDs"
  @impl Mix.Task
  def run(argv) do
    with {num, format, native} <- get_options(argv) do
      {:ok, _} = Application.ensure_all_started(:telemetry)

      :telemetry.attach_many(
        "task-handler",
        BlueLabs.telemetry_events(),
        fn name, data, metadata, _ ->
          IO.puts("#{inspect(name)} - #{inspect(data)} (#{inspect(metadata)})")
        end,
        []
      )

      1..num
      |> Enum.map(fn _ -> generate(format, native) end)
      |> Enum.each(&IO.puts(&1))
    end
  end

  defp generate(format, false) do
    BlueLabs.generate!(format: format)
  end

  defp generate(:custom_alphabet, true) do
    BlueLabs.Native.generate_alphabet()
  end

  defp generate(:hex, true) do
    BlueLabs.Native.generate_hex()
  end

  defp generate(:integer, true) do
    BlueLabs.Native.generate_integer()
  end

  defp get_options(argv) do
    argv
    |> OptionParser.parse(strict: [format: :string, native: :boolean])
    |> add_options()
  end

  defp add_options(options, result \\ {1, :custom_alphabet, false})

  defp add_options({[], [], []}, result), do: result

  defp add_options({options, [num], _}, {_, format, native}) do
    add_options({options, [], []}, {String.to_integer(num), format, native})
  end

  defp add_options({[{:native, native} | rest], args, _}, {num, format, _}) do
    add_options({rest, args, []}, {num, format, native})
  end

  defp add_options({[{:format, format} | rest], args, _}, {num, _, native})
       when format in @allowed_formats do
    add_options({rest, args, []}, {num, String.to_existing_atom(format), native})
  end
end
