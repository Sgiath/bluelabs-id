defmodule Mix.Tasks.BlueLabs.BirthdayProblem do
  @moduledoc ~S"""
  This task calculates the [Birthday Problem](https://en.wikipedia.org/wiki/Birthday_problem).

  Can be run without parameters to calculate the number for current config:

      $ mix blue_labs.birthday_problem
      76

  Or you can specify directly how much random bits (64 - time bits) should be used for calculation:

      $ mix blue_labs.birthday_problem 32
      77164

  """
  @moduledoc authors: ["Sgiath <FilipVavera@sgiath.dev>"]

  use Mix.Task

  @shortdoc "Calculate the Birthday Problem for IDs"
  @impl Mix.Task
  def run([]) do
    run([64 - Application.get_env(:blue_labs, :time_bits, 52)])
  end

  def run([random_bits]) when is_binary(random_bits) do
    run([String.to_integer(random_bits)])
  end

  def run([random_bits]) when is_integer(random_bits) do
    # "days in year" in terms of the birthday problem
    days = :math.pow(2, random_bits)
    infinite = Stream.iterate(0, &(&1 + 1))

    number =
      Enum.reduce_while(infinite, 1, fn
        n, acc when acc > 0.5 -> {:cont, acc * (days - n) / days}
        n, _ -> {:halt, n}
      end)

    IO.puts("For #{random_bits} bits the solutions is #{number}")
  end
end
