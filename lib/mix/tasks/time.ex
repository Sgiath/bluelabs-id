defmodule Mix.Tasks.BlueLabs.Time do
  @moduledoc ~S"""
  This task used for easy-to-use time generation for the library config.

  Prints single integer which represents current Unix time micorseconds. 

      $ mix blue_labs.time
      1578348210986098

  Put that in your config like this:

      config :blue_labs,
        start_time: 1578348210986098

  """
  @moduledoc authors: ["Sgiath <FilipVavera@sgiath.dev>"]

  use Mix.Task

  @shortdoc "Print current Unix time in micorseconds"
  @impl Mix.Task
  def run(_argv) do
    :blue_labs
    |> Application.get_env(:time_resolution, :microsecond)
    |> System.os_time()
    |> IO.puts()
  end
end
