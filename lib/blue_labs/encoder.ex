defmodule BlueLabs.Encoder do
  @moduledoc ~S"""
  This module is responsible for encoding raw binary ID to different formats.

  **This is internal module which is not part of the oficial API. This module can change at any
  point in the future. Use at your own risk.**
  """
  @moduledoc authors: ["Sgiath <FilipVavera@sgiath.dev"]

  import BlueLabs.Telemetry, only: [measure: 3]

  # Alphabet used for encoding
  @default_alphabet '0123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz'
  @alphabet :array.from_list(Application.compile_env(:blue_labs, :alphabet, @default_alphabet))

  # Base used for encoding
  @base :array.size(@alphabet)

  @compile {:inline,
            encode_integer: 1,
            encode_hex: 1,
            encode_base64: 1,
            encode_url: 1,
            encode_alphabet: 1,
            div_rem: 1,
            encode_char: 1}

  @doc ~S"""
  Encodes binary ID to the desired format

  ## Allowed formats

    * `:binary` - noop, returns given binary ID as is
    * `:integer` - encode binary as unsigned integer (see `encode_integer/1`)
    * `:hex` - encode binary to hexadecimal values (see `encode_hex/1`)
    * `:base64` - encode binary as Base 64 (see `encode_base64/1`)
    * `:url` - encode binary as URL safe base 64 (see `encode_url/1`)
    * `:custom_alphabet` - encode binary with custom alphabet (see `encode_alphabet/1`)

  ## Examples

      iex> BlueLabs.Encoder.encode(<<255>>)
      "4P"

      iex> BlueLabs.Encoder.encode(<<255>>, :binary)
      <<255>>

      iex> BlueLabs.Encoder.encode(<<255>>, :integer)
      255

      iex> BlueLabs.Encoder.encode(<<255>>, :hex)
      "FF"

      iex> BlueLabs.Encoder.encode(<<255>>, :base64)
      "/w"

      iex> BlueLabs.Encoder.encode(<<255>>, :url)
      "_w"

      iex> BlueLabs.Encoder.encode(<<255>>, :custom_alphabet)
      "4P"

  """
  @spec encode(id :: binary(), format :: BlueLabs.format()) :: BlueLabs.t()
  def encode(id, format \\ :custom_alphabet)

  def encode(id, :binary) when is_binary(id) do
    id
  end

  def encode(id, :integer) when is_binary(id) do
    measure :encode, format: :integer do
      encode_integer(id)
    end
  end

  def encode(id, :hex) when is_binary(id) do
    measure :encode, format: :hex do
      encode_hex(id)
    end
  end

  def encode(id, :base64) when is_binary(id) do
    measure :encode, format: :base64 do
      encode_base64(id)
    end
  end

  def encode(id, :url) when is_binary(id) do
    measure :encode, format: :url do
      encode_url(id)
    end
  end

  def encode(id, :custom_alphabet) when is_binary(id) do
    measure :encode, format: :custom_alphabet do
      encode_alphabet(id)
    end
  end

  @doc ~S"""
  Encodes binary into integer

  Uses `:binary.decode_unsigned/1`
  """
  @spec encode_integer(id :: binary()) :: integer()
  def encode_integer(id) when is_binary(id) do
    :binary.decode_unsigned(id)
  end

  @doc ~S"""
  Encodes binary into hexadecimal representation

  Uses `Base.encode16/2`
  """
  @spec encode_hex(id :: binary()) :: String.t()
  def encode_hex(id) when is_binary(id) do
    Base.encode16(id)
  end

  @doc ~S"""
  Encodes binary into Base64

  Uses `Base.encode64/2`
  """
  @spec encode_base64(id :: binary()) :: String.t()
  def encode_base64(id) when is_binary(id) do
    Base.encode64(id, padding: false)
  end

  @doc ~S"""
  Encodes binary into URL safe Base64

  Uses `Base.url_encode64/2`
  """
  @spec encode_url(id :: binary()) :: String.t()
  def encode_url(id) when is_binary(id) do
    Base.url_encode64(id, padding: false)
  end

  @doc ~S"""
  Encodes binary with custom alphabet

  First encodes to integer and than recursively encodes char by char by deviding and computing
  reminder
  """
  @spec encode_alphabet(id :: binary() | integer()) :: String.t()
  def encode_alphabet(id) when is_binary(id) do
    id
    |> encode_integer()
    |> encode_alphabet()
  end

  def encode_alphabet(id) when is_integer(id) do
    encode_alphabet_rec(id)
  end

  # Recursively encode integer with custom alphabet
  defp encode_alphabet_rec(number, result \\ [])

  defp encode_alphabet_rec(0, result) when is_list(result) do
    List.to_string(result)
  end

  defp encode_alphabet_rec(number, result) when is_integer(number) and is_list(result) do
    with {d, r} <- div_rem(number) do
      encode_alphabet_rec(d, [encode_char(r) | result])
    end
  end

  # Get coresponding char from the alphabet
  defp encode_char(number) when is_integer(number) do
    :array.get(number, @alphabet)
  end

  # Get result of division and reminder in one step
  defp div_rem(number) when is_integer(number) do
    {div(number, @base), rem(number, @base)}
  end
end
