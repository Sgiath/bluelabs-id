defmodule BlueLabs.Native do
  @moduledoc ~S"""
  Native implementation in Rust

  This module runs native code implemented in Rust. This is aiming to be public API but at this
  stage it is very much **experimental** and can change any time.

  This API is a lot faster than Elixir implementation so it can be used in situations where great
  throughput is required.

  ## Configuration

  Currently the native implementation doesn't take into account Elixir configuration and is
  hardcoded to generate IDs equivalent to this config:

      config :blue_labs,
        time_resolution: :second,
        time_bits: 32,
        alphabet: '0123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz',
        start_time: 0,
        telemetry: false

  """
  @moduledoc authors: ["Sgiath <FilipVavera@sgiath.dev>"]
  @moduledoc since: "0.2.0"

  use Rustler, otp_app: :blue_labs

  @doc """
  Generate integer representation of the ID in native code
  """
  @spec generate_integer() :: integer()
  def generate_integer, do: :erlang.nif_error(:nif_not_loaded)

  @doc """
  Generate hex representation of the ID in native code
  """
  @spec generate_hex() :: String.t()
  def generate_hex, do: :erlang.nif_error(:nif_not_loaded)

  @doc """
  Generate custom alphabet representation of the ID in native code
  """
  @spec generate_alphabet() :: String.t()
  def generate_alphabet, do: :erlang.nif_error(:nif_not_loaded)
end
