defmodule BlueLabs.Generator do
  @moduledoc ~S"""
  This module is responsible for generating raw binary ID. The algorithm and rationale behind it
  is described in [Algorithm](algorithm.html) and [Rationale](rationale.html) pages.

  **This is internal module which is not part of the oficial API. This module can change at any
  point in the future. Use at your own risk.**
  """
  @moduledoc authors: ["Sgiath <FilipVavera@sgiath.dev>"]

  @time_resolution Application.compile_env(:blue_labs, :time_resolution, :microsecond)

  # Begining of the series - Unix time in microseconds
  @start_time Application.compile_env(:blue_labs, :start_time, 1_578_044_200_335_382)

  # Number of bits used for time component of the ID
  @time_bits Application.compile_env(:blue_labs, :time_bits, 52)

  # Number of bits used for process ID component of the ID
  @random_bits 64 - @time_bits

  # Max integer in 64 bits
  # String.to_integer("FFFFFFFFFFFFFFFF", 16)
  @max_64_integer 18_446_744_073_709_551_615

  @compile {:inline, get_time: 0, get_random: 0}

  @doc ~S"""
  Generates new ID.

  Implements algorithm described [here](algorithm.html).
  """
  @spec generate() :: <<_::64>>
  def generate do
    <<get_time()::@time_bits, get_random()::@random_bits>>
  end

  # Get number of micorseconds from the begining of the series
  defp get_time do
    System.os_time(@time_resolution) - @start_time
  end

  # Get random bytes
  defp get_random do
    :rand.uniform(@max_64_integer)
  end
end
