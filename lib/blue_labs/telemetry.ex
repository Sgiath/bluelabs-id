defmodule BlueLabs.Telemetry do
  @moduledoc ~S"""
  This module contains macro wich, depending on the config, adds :telemetry measurements to the
  code.

  **This is internal module which is not part of the oficial API. This module can change at any
  point in the future. Use at your own risk.**
  """
  @moduledoc authors: ["Sgiath <FilipVavera@sgiath.dev"]

  @doc ~S"""
  This macro will check if the telemetry is enabled in config. If yes it will include telemetry
  code. If telemetry is disabled (default) it won't do anything.

  ## Example

      def my_fn do
        measure(:generate) do
          ... # do some work here
        end
      end

  If telemetry is enabled it will result in folowing code:

      def my_fn do
        start_time = System.monotonic_time(:nanosecond)
        :telemetry.execute([:blue_labs, :generate, :start], %{time: start_time})

        result = ... # do some work here

        end_time = System.monotonic_time(:nanosecond)

        :telemetry.execute([:blue_labs, :generate, :stop], %{
          time: end_time,
          duration: end_time - start_time
        })

        result
      end

  If telemetry is disabled it will compile this code:

      def my_fn do
        ... # do some work here
      end

  """
  defmacro measure(event, metadata \\ [], do: expression) do
    if Application.get_env(:blue_labs, :telemetry, false) do
      metadata =
        metadata
        |> Enum.into(%{})
        |> Macro.escape()

      quote do
        start_time = System.monotonic_time(:nanosecond)

        :telemetry.execute(
          [:blue_labs, unquote(event), :start],
          %{time: start_time},
          unquote(metadata)
        )

        result = unquote(expression)

        end_time = System.monotonic_time(:nanosecond)

        :telemetry.execute(
          [:blue_labs, unquote(event), :stop],
          %{
            time: end_time,
            duration: end_time - start_time
          },
          unquote(metadata)
        )

        result
      end
    else
      quote do
        unquote(expression)
      end
    end
  end

  @doc """
  Helper function to list all available events

  See `BlueLabs.telemetry_events/0`
  """
  def events do
    [
      [:blue_labs, :generate, :start],
      [:blue_labs, :generate, :stop],
      [:blue_labs, :encode, :start],
      [:blue_labs, :encode, :stop]
    ]
  end
end
