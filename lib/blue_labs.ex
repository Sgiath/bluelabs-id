defmodule BlueLabs do
  @moduledoc ~S"""
  This page describes how to use this library, if you want to know more about the rationale behind
  it and how exactly are IDs generated see [Overview](overview.html) page.

  ## Usage

  This library provides two functions as it's public API `BlueLabs.generate/1` and
  `BlueLabs.generate!/1`. They both do the same the only difference is the shape of the return
  type (as ! hints).

      iex> BlueLabs.generate!() 
      "4ydxxnM9u"

      iex> BlueLabs.generate()
      {:ok, "4ydxxnM9u"}

  It also provides one helper function for telemetry events `BlueLabs.telemetry_events/0`.

  ## Configuration

  See [Configuration](configuration.html) page.
  """
  @moduledoc authors: ["Sgiath <FilipVavera@sgiath.dev>"]

  import BlueLabs.Telemetry, only: [measure: 2]

  alias BlueLabs.{Encoder, Generator}

  @typedoc ~S"""
  Allowed formats for generated ID

  It could be:
    * raw 64-bit binary
    * string - hex encoded, Base64 encoded, URL encoded and custom alphabet encoded
    * integer - integer representation of binary
  """
  @type t() :: <<_::64>> | String.t() | integer()

  @typedoc ~S"""
  Atoms describing allowed formats
  """
  @type format() :: :binary | :integer | :hex | :base64 | :url | :custom_alphabet

  @typedoc ~S"""
  All available options for the `generate/1` and `generate!/1` function call

  Currently only one option is allowed - `:format`
  """
  @type option() :: {:format, format()}

  @typedoc ~S"""
  All errors that can be returned from `generate/1` function

  Currently only one error is returned - `:invalid_format`
  """
  @type error() :: :invalid_format

  @allowed_formats [:binary, :integer, :hex, :base64, :url, :custom_alphabet]
  @default_format :custom_alphabet

  @doc ~S"""
  Generate random ID

  ## Options

    * `:format` - can be `:custom_alphabet` (default), `:binary`, `:integer`, `:hex`, `:base64`,
      `:url`

  ## Examples

      iex> BlueLabs.generate()
      {:ok, "6mnMnNH8m"}

      iex> BlueLabs.generate(format: :binary)
      {:ok, <<0, 4, 14, 124, 46, 191, 48, 139>>}

      iex> BlueLabs.generate(format: :integer)
      {:ok, 1142007135104887}

      iex> BlueLabs.generate(format: :hex)
      {:ok, "00040EC283BB2136"}

      iex> BlueLabs.generate(format: :base64)
      {:ok, "AAQPGspRehE"}

      iex> BlueLabs.generate(format: :url)
      {:ok, "AAQyOmUIXhs"}

  """
  @spec generate(options :: [option()]) :: {:ok, t()} | {:error, error(), String.t()}
  def generate(options \\ []) do
    case Keyword.get(options, :format, @default_format) do
      format when format in @allowed_formats ->
        measure :generate do
          {:ok, Encoder.encode(Generator.generate(), format)}
        end

      format ->
        {:error, :invalid_format, format}
    end
  end

  @doc ~S"""
  Same as `generate/1` but returns raw ID or raises instead of `:ok` / `:error` tuple

  ## Examples

      iex> BlueLabs.generate!()
      "6mnMnNH8m"

      iex> BlueLabs.generate!(format: :binary)
      <<0, 4, 14, 124, 46, 191, 48, 139>>

      iex> BlueLabs.generate!(format: :integer)
      1142007135104887

      iex> BlueLabs.generate!(format: :hex)
      "00040EC283BB2136"

      iex> BlueLabs.generate!(format: :base64)
      "AAQPGspRehE"

      iex> BlueLabs.generate!(format: :url)
      "AAQyOmUIXhs"

  """
  @spec generate!(options :: [option()]) :: t()
  def generate!(options \\ []) do
    case generate(options) do
      {:ok, id} ->
        id

      {:error, :invalid_format, format} ->
        raise ArgumentError, message: "Invalid format #{inspect(format)}"
    end
  end

  @doc ~S"""
  Get all telemetry events library fires.

  This is helper function for your telemetry setup. You can use it like:

      :telemetry.attach_many("blue-labs", BlueLabs.telemetry_events(), &handle/4, [])

  """
  @spec telemetry_events() :: [:telemetry.event_name()]
  defdelegate telemetry_events, to: BlueLabs.Telemetry, as: :events
end
