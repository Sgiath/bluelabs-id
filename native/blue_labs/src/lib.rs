use rand::Rng;
use std::time::SystemTime;

/**
 * Generate new id as unsigned 64 bit number
 */
fn generate() -> u64 {
    // get current Unix time in seconds
    let time = match SystemTime::now().duration_since(SystemTime::UNIX_EPOCH) {
        Ok(epoch) => epoch.as_secs(),
        // TODO: handle error better
        Err(_) => 0,
    };

    // generate random unsigned 64 number
    let random = rand::thread_rng().gen::<u64>();

    // get first 32 least significant bits from time
    let time_bits = time & 0xFFFFFFFF;

    // ge first 32 least significant bits from random number
    let random_bits = random & 0xFFFFFFFF;

    // shift time by 32 bits to left and append random bits at the end
    return time_bits << 32 | random_bits;
}

/**
 * Generate new ID
 */
#[rustler::nif]
fn generate_integer() -> u64 {
    return generate();
}

/**
 * Generate new ID and convert it to hex format
 */
#[rustler::nif]
fn generate_hex() -> String {
    return format!("{:x}", generate());
}

/**
 * Generate new ID and convert it to custom alphabet
 */
#[rustler::nif]
fn generate_alphabet() -> String {
    // hardcoded alphabet
    // TODO: make it configurable
    let alphabet = [
        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H',
        'J', 'K', 'L', 'M', 'N', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b',
        'c', 'd', 'e', 'f', 'g', 'h', 'i', 't', 'u', 'v', 'w', 'x', 'y', 'z',
    ];

    // base of the encoding - length of the alphabet
    let base = alphabet.len();

    // final encoded ID
    let mut result: String = "".to_string();

    // generate ID and compute first division + reminder
    let mut dr = div_rem(generate(), base);

    // encode ID until result of division is 0
    while dr.0 != 0 {
        // insert correct char at the 1. possition in the ID
        result.insert(0, alphabet[dr.1]);

        // compute new division + reminder
        dr = div_rem(dr.0, base);
    }

    return result;
}

/**
 * Comute division and reminder in one step
 */
fn div_rem(x: u64, y: usize) -> (u64, usize) {
    return ((x / y as u64), (x % y as u64) as usize);
}

// export Elixir NIFs
rustler::init!(
    "Elixir.BlueLabs.Native",
    [generate_integer, generate_hex, generate_alphabet]
);
