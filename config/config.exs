import Config

config :blue_labs,
  time_resolution: :microsecond,
  time_bits: 52,
  alphabet: '0123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnpqrstuvwxyz',
  start_time: 1_578_044_200_335_382,
  telemetry: false
