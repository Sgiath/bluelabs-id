defmodule Bench.ListAccessTime do
  @alphabet '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
  @list @alphabet
  @array :array.from_list(@alphabet)
  @len length(@alphabet)
  @size 1..20

  @doc """
  Generate different inputs
  """
  @spec input(:first | :last | :random) :: [integer()]
  def input(:first) do
    Enum.map(@size, fn _ -> 0 end)
  end

  def input(:last) do
    Enum.map(@size, fn _ -> @len - 1 end)
  end

  def input(:random) do
    Enum.map(@size, fn _ -> :rand.uniform(@len - 1) end)
  end

  @doc """
  Simulate access for the list
  """
  @spec list([integer()]) :: [integer()]
  def list(input) do
    Enum.map(input, &Enum.at(@list, &1))
  end

  @doc """
  Simulate access for the array
  """
  @spec array([integer()]) :: [integer()]
  def array(input) do
    Enum.map(input, &:array.get(&1, @array))
  end
end

Benchee.run(
  %{
    "List" => &Bench.ListAccessTime.list/1,
    ":array" => &Bench.ListAccessTime.array/1
  },
  inputs: %{
    "first" => Bench.ListAccessTime.input(:first),
    "last" => Bench.ListAccessTime.input(:last),
    "random" => Bench.ListAccessTime.input(:random)
  },
  formatters: [{Benchee.Formatters.Console, extended_statistics: true}]
)
