Benchee.run(
  %{
    "native_integer" => fn -> BlueLabs.Native.generate_integer() end,
    "native_hex" => fn -> BlueLabs.Native.generate_hex() end,
    "native_alphabet" => fn -> BlueLabs.Native.generate_alphabet() end,
    "binary" => fn -> BlueLabs.generate(format: :binary) end,
    "integer" => fn -> BlueLabs.generate(format: :integer) end,
    "hex" => fn -> BlueLabs.generate(format: :hex) end,
    "base64" => fn -> BlueLabs.generate(format: :base64) end,
    "url" => fn -> BlueLabs.generate(format: :url) end,
    "custom_alphabet" => fn -> BlueLabs.generate(format: :custom_alphabet) end
  },
  formatters: [{Benchee.Formatters.Console, extended_statistics: true}]
)
