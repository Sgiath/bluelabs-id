defmodule Bench.ListVsBinary do
  @id "Usnd4scDF"

  @doc """
  Generates input for benchamrk
  """
  @spec input(size :: integer()) :: charlist()
  def input(size \\ 1) when is_integer(size) do
    1..size
    |> Enum.reduce("", fn _, acc -> @id <> acc end)
    |> String.to_charlist()
  end

  @doc """
  Simulates encoding function using list as accumulator
  """
  @spec list(input :: charlist(), output :: charlist()) :: String.t()
  def list(input, output \\ [])

  def list([], output) do
    List.to_string(output)
  end

  def list([val | input], output) do
    list(input, [val | output])
  end

  @doc """
  Simulates encoding function using binary as accumulator
  """
  @spec binary(input :: charlist(), output :: binary()) :: String.t()
  def binary(input, output \\ "")

  def binary([], output) do
    output
  end

  def binary([val | input], output) do
    binary(input, <<val::size(8), output::binary>>)
  end
end

Benchee.run(
  %{
    "List" => &Bench.ListVsBinary.list/1,
    "binary" => &Bench.ListVsBinary.binary/1
  },
  inputs: %{
    "Small" => Bench.ListVsBinary.input(),
    "Large" => Bench.ListVsBinary.input(20)
  },
  formatters: [{Benchee.Formatters.Console, extended_statistics: true}]
)
