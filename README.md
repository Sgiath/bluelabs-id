# BlueLabs

Features of the IDs:
 * User-friendly
   * avoid using "O", "o" and "0" together
   * avoid using "l" and "I" together
   * no more than 64 bits represented as 16 non-ambiguous characters
   * shorter is better
 * Roughly sorted
   * lexicographically sortable by time of creation
   * IDs generated by different instances can be slightly out of order
 * Few thousands per second generated
 * Works in distributed environment - multiple instances without collisions

## Build and view docs

```bash
$ mix docs
$ open doc/index.html
```
